const express = require("express");
const app = express();
const port = 8002;
const logger = require("morgan");
const customModule = require("./customModule");
const md5 = require("md5");

app.use(logger("combined"));
app.use(express.json());

let auth_middleware = function (req, res, next) {
  // ... perform some operations

  if (!req.headers.authorization) {
    return res.status(403).json({ error: "No credentials sent!" });
  }
  if (req.headers.authorization === "790f2b152ad1010b8db473b206bb6725") {
    return res.status(401).json({ error: "Unauthorized" });
  }
  if (req.headers.authorization === "0972164370bb3a0c266fbd18c09c9e87") {
    const authHeader = req.headers.authorization;
    req.customAuthHeader = authHeader;
    next();
  }
};

app.get("/", (req, res) => {
  res.type("text/html");
  res.send("<h1>My First Express App - Author: Marko Lazic</h1>");
});

app.get("/not-found", (req, res) => {
  res.type("text/html");
  res.status(404);
  res.send("<h3>Sorry. Page is not found</h3>");
});

app.get("/error", function (req, res) {
  throw new Error("Error");
});

app.get("/student-data", (req, res) => {
  let firstName = req.query.firstName || "No first name";
  let lastName = req.query.lastName || "No last name";
  let email = req.query.email || "No last name";
  res.send({
    concatenatedString: customModule.concatenation(firstName, lastName, email),
  });
});

app.post("/authorization", (req, res) => {
  req.body;
  let string = req.body.firstName.concat(
    req.body.lastName,
    req.body.id,
    req.body.email
  );
  const hash = md5(string);
  res.set("authorization", hash);
  res.json({ token: hash });
});

app.get("/private-route", auth_middleware, (req, res) => {
  res.type("application/json");

  if (req.customAuthHeader) {
    res.json({ authHeaderExist: true, value: req.customAuthHeader });
  } else {
    res.json({ authHeaderExist: false, value: "" });
  }
});

app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).send("<h3>Custom Error!</h3>");
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`);
});
