const { json } = require("express");

module.exports = {
  concatenation: function (firstName, lastName, email) {
    const string = firstName.concat(lastName, email);
    return string;
  },
};
